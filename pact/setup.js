// ./pact/setup.js

import { Pact } from '@pact-foundation/pact';
import { resolve } from 'path';

global.port = 8877;
global.provider = new Pact({
    cors: true,
    port: global.port,
    log: resolve(process.cwd(), 'logs', 'pact.log'),
    loglevel: 'info',
    dir: resolve(process.cwd(), 'pacts'),
    spec: 2,
    pactfileWriteMode: 'update',
    consumer: 'silesia-film-frontend',
    provider: 'silesia-film-backend',
    host: '127.0.0.1'
});