// ./pact/publish.js
const publisher = require('@pact-foundation/pact-node');
const path = require('path');
const { execSync } = require('child_process');

function consumerVersion() {
    let version = '1.0.0'
    if (process.env.GIT_COMMIT == null) {
        version = execSync('git rev-parse --verify HEAD', (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return version;
            }
            return stdout.trim();
        }).toString().trim();
    } else {
        version = process.env.GIT_COMMIT;
    }
    return version;
}

let opts = {
    providerBaseUrl: 'http://localhost:8080',
    pactFilesOrDirs: [path.resolve(process.cwd(), 'pacts')],
    pactBroker: process.env.PACT_BROKER_URL,
    pactBrokerUsername: process.env.PACT_USERNAME,
    pactBrokerPassword: process.env.PACT_PASSWORD,
    consumerVersion: consumerVersion()
};

publisher.publishPacts(opts).then(() => console.log("Pacts successfully published"));