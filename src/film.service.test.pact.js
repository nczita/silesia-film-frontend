// film.service.test.pact.js
import FilmService from './film.service';
import * as Pact from '@pact-foundation/pact';
import Film from './film';

describe('FilmService API', () => {

    const filmService = new FilmService('http://localhost', global.port);

    describe('createFilm()', () => {

        beforeEach((done) => {
            const contentTypeJsonMatcher = Pact.Matchers.term({
                matcher: "application\\/json(; charset=utf-8)?",
                generate: "application/json; charset=utf-8"
            });

            global.provider.addInteraction({
                state: 'provider allows film creation',
                uponReceiving: 'a POST request to create a film',
                withRequest: {
                    method: 'POST',
                    path: '/films',
                    headers: {
                        'Accept': contentTypeJsonMatcher,
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: new Film(null, 'Man of Steel', '2013', 7.1)
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: Pact.Matchers.somethingLike(
                        new Film('08029fe6-ad5c-4ea5-baf3-325110a6f847', 'Man of Steel', '2013', 7.1))
                }
             }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            filmService.createFilm(new Film(null, 'Man of Steel', '2013', 7.1))
            .then(response => {
                const film = response.data;
                expect(film.id).toEqual('08029fe6-ad5c-4ea5-baf3-325110a6f847');
                expect(film.title).toEqual('Man of Steel');
                expect(film.year).toEqual('2013');
                expect(film.rating).toEqual(7.1);
            })
            .then(() => {
                global.provider.verify()
                    .then(() => done(), error => {
                        done.fail(error)
                    })
            });
        });
    });

    describe('getFilm()', () => {

        beforeEach((done) => {
            const contentTypeJsonMatcher = Pact.Matchers.term({
                matcher: "application\\/json(; charset=utf-8)?",
                generate: "application/json; charset=utf-8"
            });

            global.provider.addInteraction({
                state: 'provider have film',
                uponReceiving: 'a GET request to get a film description',
                withRequest: {
                    method: 'GET',
                    path: '/films/08029fe6-ad5c-4ea5-baf3-325110a6f847',
                    headers: {
                        'Accept': contentTypeJsonMatcher
                    }
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: Pact.Matchers.somethingLike(
                        new Film('08029fe6-ad5c-4ea5-baf3-325110a6f847', 'Man of Steel', '2013', 7.1))
                }
             }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            filmService.getFilm('08029fe6-ad5c-4ea5-baf3-325110a6f847')
            .then(response => {
                const film = response.data;
                expect(film.id).toEqual('08029fe6-ad5c-4ea5-baf3-325110a6f847');
                expect(film.title).toEqual('Man of Steel');
                expect(film.year).toEqual('2013');
                expect(film.rating).toEqual(7.1);
            })
            .then(() => {
                global.provider.verify()
                    .then(() => done(), error => {
                        done.fail(error)
                    })
            });
        });
    });
});