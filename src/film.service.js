// film.service.js

import adapter from 'axios/lib/adapters/http';
const axios = require('axios');

class FilmService {
    constructor(baseUrl, port){
        this.baseUrl = baseUrl;
        this.port = port;
    }

    createFilm(film) {
        return axios.request({
            method: 'POST',
            url: `/films`,
            baseURL: `${this.baseUrl}:${this.port}`,
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            data: film
        }, adapter);
    };

    getFilm(filmId) {
        return axios.request({
            method: 'GET',
            url: `/films/${filmId}`,
            baseURL: `${this.baseUrl}:${this.port}`,
            headers: {
                'Accept': 'application/json; charset=utf-8'
            }
        }, adapter);
    };

}

export default FilmService;