// comment.js

class Comment {
    constructor(id, commentedId, author, text) {
        this.id = id;
        this.commentedId = commentedId;
        this.author = author;
        this.text = text;
    }
}

export default Comment;