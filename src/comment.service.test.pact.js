// comments.service.test.pact.js
import CommentsService from './comment.service';
import * as Pact from '@pact-foundation/pact';
import Comment from './comment';

describe('CommentsService API', () => {

    const commentsService = new CommentsService('http://localhost', global.port);

    describe('getComments()', () => {

        beforeEach((done) => {
            const contentTypeJsonMatcher = Pact.Matchers.term({
                matcher: "application\\/json(; charset=utf-8)?",
                generate: "application/json; charset=utf-8"
            });

            global.provider.addInteraction({
                state: 'provider have comments for film',
                uponReceiving: 'a GET request to get a comments',
                withRequest: {
                    method: 'GET',
                    path: '/comments/08029fe6-ad5c-4ea5-baf3-325110a6f847',
                    headers: {
                        'Accept': contentTypeJsonMatcher
                    }
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: Pact.Matchers.eachLike(new Comment('ac55a524-9b0b-49e6-aeb8-9aa8a8ffd250',
                            '08029fe6-ad5c-4ea5-baf3-325110a6f847',
                            'QualityExites',
                            'Great movie!'))
                }
             }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            commentsService.getComments('08029fe6-ad5c-4ea5-baf3-325110a6f847')
            .then(response => {
                const comments = response.data;
                expect(comments[0].id).toEqual('ac55a524-9b0b-49e6-aeb8-9aa8a8ffd250');
                expect(comments[0].commentedId).toEqual('08029fe6-ad5c-4ea5-baf3-325110a6f847');
                expect(comments[0].author).toEqual('QualityExites');
                expect(comments[0].text).toEqual('Great movie!');
            })
            .then(() => {
                global.provider.verify()
                    .then(() => done(), error => {
                        done.fail(error)
                    })
            });
        });
    });
});