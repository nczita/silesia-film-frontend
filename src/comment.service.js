// comment.service.js

import adapter from 'axios/lib/adapters/http';
const axios = require('axios');


class CommentService {

    constructor(baseUrl, port){
        this.baseUrl = baseUrl;
        this.port = port;
    }

    getComments(commentedId) {
        return axios.request({
            method: 'GET',
            url: `/comments/${commentedId}`,
            baseURL: `${this.baseUrl}:${this.port}`,
            headers: {
                'Accept': 'application/json; charset=utf-8',
            }
        }, adapter);
    };

}

export default CommentService;