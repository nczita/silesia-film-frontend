# SilesiaFilm

SilesiaFilm application shows shows statistics and comments of various film productions.

This is a frontend application of SilesiaFilm system.

## Milestone 1

In this milestone our app should allow user to:
- add new film
- get basic info about specific film
- get comments about specific film

## Milestone 2

In this milestone our app should allow user to:
- get list of added films
- get rating of films
- extend description of film object with a film poster

## Installation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

I have added `axios` and `pact-foundation/pact` dependencies:

```
$ yarn add axios
$ yarn add -D @pact-foundation/pact
```

## Issues

Problem with overwriting pact logs by subsequent tests:
- `tail -f logs/pact.log | tee logs/all_pact.log`

Problem with running tests without user interactions (CI mode):
- https://github.com/facebook/create-react-app/issues/784


## Acknowledges

Tom Hombergs [Implementing a Consumer-Driven Contract for a React App with Pact and Jest](https:    //reflectoring.io/pact-react-consumer/)

Peter Bengtsson [How to create-react-app with Docker](https://www.peterbe.com/plog/how-to-create-react-app-with-docker)

[Pact documentation](https://docs.pact.io/implementation_guides/javascript)

[Pact JS](https://github.com/pact-foundation/pact-js)
